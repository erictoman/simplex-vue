from analizadorProblema import analizadorProblemaSimplex as aPS
import pandas as pd
import numpy as np

class solverSimplex():
    def __init__(self, data):
        analizador = aPS(data)
        self.status = False
        self.MatricesInfo = []
        self.limitIterations = 15
        self.Indeterminado = False
        self.objetivo = analizador.get_Objetivo()
        self.Matrices = [analizador.get_First_Matrix()]

    def solveProblem(self):
        try:
            i = 0
            while not self.status and not self.Indeterminado:       #Realizar iteraciones hasta que no se llegue las condiciones de parada de simplex 2
                matriz = self.Matrices[-1]                          #Se selecciona la ultima matriz del problema
                indicesfilasvar = matriz.index.values[:-3]          #Se seleccionan los indices diferentes a Cj, Zj, Cj-Zj
                if( matriz['Sol'].loc[indicesfilasvar].min() < 0 ): #Comprobar que no haya limites menores a 0 en el vector solucion de la matriz
                    self.iteraSimplex3()
                else:
                    self.iteraSimplex2()
                if( i < self.limitIterations):                      #Revisamos si el limites de iteraciones no se ha superado
                    i += 1
                else:
                    self.Indeterminado = True                       #Si s supero el numero maximo de iteraciones se indetermina el problema
        except:
            print("Wrong data")
            pass

    def iteraSimplex2(self):
        actualIndex = len(self.Matrices) - 1                #Obtener el numero de la iteracion
        try:
            self.MatricesInfo[actualIndex]                  #Se busca informacion de la iteracion.
        except:
            self.MatricesInfo.append({'notChooseAgain': [], 'possibleChoose': []})  #Si no existe información se inicializa

        matriz = self.Matrices[actualIndex]                 #Se selecciona la ultima matriz del problema
        indicesfilasvar = matriz.index.values[:-3]          #Se seleccionan los indices diferentes a Cj, Zj, Cj-Zj
        CjZj = matriz.loc['Cj-Zj']                          #Seleccionar la fila Cj - Zj
        if(self.objetivo == 'Maximizar'):
            candidatosColumna = CjZj[ CjZj > 0 ]            #Filtramos los valores positivos en Cj - Zj
            if len(candidatosColumna) > 0:                  #Si existe un valor positivo se busca el id de la Columna
                idchosencolumn = candidatosColumna.idxmax() #Se elige el valor mas positivo
            else:                                           #Si no existe un valor positivo no se itera de nuevo y se avisa que termino el metodo
                self.status = True

        else:
            candidatosColumna = CjZj[ CjZj < 0 ]            #Filtramos los valores negativos en Cj - Zj
            if len(candidatosColumna) > 0:                  #Si existe un valor negativo se busca el id de la Columna
                idchosencolumn = candidatosColumna.idxmin() #Se elige el valor mas negativo
            else:                                           #Si no existe un valor negativo no se itera de nuevo y se avisa que termino el metodo
                self.status = True

        if not self.status:                                             #Si existe un id de la Columna, se busca el id de la Fila para iterar de nuevo
            chosencolumn = matriz[idchosencolumn].loc[indicesfilasvar]  #Se selecciona la columna elegida y sus valores en las filas de variable
            div = matriz['Sol'].loc[indicesfilasvar] / chosencolumn     #Se divide la columna solucion entre la columna elegida
            div = div.replace([np.inf, -np.inf], np.nan)                #Eliminar valores de infinito
            div = div.fillna(0)                                         #Eliminar indeterminaciones en la operaciones
            candidatosFila = div[ div > 0 ]                             #Filtramos los valores positivos en Cj - Zj
            if len(candidatosFila) > 0:                                 #Si existe un valor positivo se busca el id minimo de la Columna
                idchosenrow = candidatosFila.idxmin()
                self.createNewMatrix(idchosencolumn,  idchosenrow)      #Crear la nueva matriz
            else:                                                       #Si no existe un valor positivo hay dos caminos
                if actualIndex > 0:                                     #Si no es la primera matriz
                    temporalObject = self.MatricesInfo[actualIndex - 1]     #Accedemos a la informacion de la iteracion anterior.
                    if len(temporalObject['possibleChoose']) > 0:           #Si existen posibles variables a seleccionar para elegir un nuevo camino en la iteracion anterior
                        self.MatricesInfo = self.MatricesInfo[0:actualIndex] #Se elimina la informacion matriz actual
                        self.Matrices = self.Matrices[0:-1] #Se elimina la matriz analizada en esta iteracion
                    else:
                        self.Indeterminado = True #Se indetermina el problema
                else:
                    self.Indeterminado = True #Se indetermina el problema


    def iteraSimplex3(self):
        actualIndex = len(self.Matrices) - 1    #Obtener el numero de la iteracion
        try:
            self.MatricesInfo[actualIndex]      #Se busca informacion de la iteracion.
        except:
            self.MatricesInfo.append({'notChooseAgain': [], 'possibleChoose': []})  #Si no existe información se inicializa
        temporalObject = self.MatricesInfo[actualIndex]

        matriz = self.Matrices[actualIndex]                         #Se selecciona la ultima matriz del problema
        indicesfilasvar = matriz.index.values[:-3]                  #Se seleccionan los indices diferentes a Cj, Zj, Cj-Zj
        idchosenrow = matriz['Sol'].loc[indicesfilasvar].idxmin()   #ID de la fila con el valor mas negativo
        chosenrow = matriz.loc[ [idchosenrow] ]                     #Seleccionar la fila elegida
        div = matriz.loc['Cj-Zj'] / chosenrow                       #Dividir Cj - Zj / fila elegida
        div = div.drop(temporalObject['notChooseAgain'], axis=1)    #Eliminar las filas ya elegidas
        div = div.replace([np.inf, -np.inf], np.nan)                #Eliminar valores de infinito
        div = div.fillna(0)                                         #Eliminar indeterminaciones en la operaciones
        possiblechosenrows = div[div != 0].loc[idchosenrow]         #Obtener las filas candidatas
        if len(possiblechosenrows) > 0:
            idchosencolumn = possiblechosenrows.sub(0).abs().idxmin()   #Elegir el valor mas cercano a 0
            temporalObject['notChooseAgain'].append(idchosencolumn)     #Agregar la fila elegida a las ya elegidas
            minval = possiblechosenrows.sub(0).abs().min()              #Obtener el valor minimo
            a = possiblechosenrows.sub(0).abs()                         
            temporalObject['possibleChoose'] = a[a == minval].index[1:] #Obtener los ids de las filas que se pueden eligir
            self.createNewMatrix(idchosencolumn, idchosenrow)           #Crear nueva matriz
        else:
            if actualIndex > 0:                                     #Si no es la primera matriz
                temporalObject = self.MatricesInfo[actualIndex - 1]     #Accedemos a la informacion de la iteracion anterior.
                if len(temporalObject['possibleChoose']) > 0:           #Si existen posibles variables a seleccionar para elegir un nuevo camino en la iteracion anterior
                    self.MatricesInfo = self.MatricesInfo[0:actualIndex] #Se elimina la informacion matriz actual
                    self.Matrices = self.Matrices[0:-1] #Se elimina la matriz analizada en esta iteracion
                else:
                    self.Indeterminado = True #Se indetermina el problema
            else:
                self.Indeterminado = True #Se indetermina el problema

    def createNewMatrix(self, idchosencolumn, idchosenrow):
        matriz = self.Matrices[-1]                                      #Se selecciona la ultima matriz del problema
        chosenrow = matriz.loc[ idchosenrow ]                           #Se selecciona la fila elegida
        divisorfactor = matriz[idchosencolumn].loc[idchosenrow]         #Se determina el factor de division para volver '1' el valor [Columna][Fila]
        newRow = chosenrow / divisorfactor                              #Se determina la nueva fila como Fila elegida / factor de division

        nuevamatriz = matriz.copy()                                             #Copiar la ultima matriz para modificarla
        indicesfilasvar = nuevamatriz.index.values[:-3]                         #Se seleccionan los indices diferentes a Cj, Zj, Cj-Zj
        nuevamatriz.loc[idchosenrow] = newRow                                   #La fila elegida se iguala con la nueva
        nuevamatriz = nuevamatriz.rename(index={idchosenrow: idchosencolumn})   #Cambiar el indice de la fila elegida por el de la columna elegida
        
        nuevaZj = 0                                                             #Variable para acumular el valor de Zj
        valorCj = nuevamatriz[idchosencolumn].loc['Cj']                         #Determinar el valor del indice de la columna elegida en Cj
        nuevaZj = nuevaZj + nuevamatriz.loc[idchosencolumn] * valorCj           #Sumar a nuevaZj la multiplicacion de la fila nueva por su valor en Cj
        
        for indicefila in indicesfilasvar:                                                      #Realizar por cada fila de variables en la matriz
            if(indicefila != idchosenrow):                                                      #Si la fila es distinta a la elegida
                factor = nuevamatriz[idchosencolumn].loc[indicefila]                            #Determinar el factor para multiplicar la nueva fila y realizar 0 la columna
                nuevamatriz.loc[indicefila] = nuevamatriz.loc[indicefila] + newRow * (-factor)  #Realizar operacion fila(x) - filanueva * factor
                valorCj = nuevamatriz[indicefila].loc['Cj']                                     #Determinar el valor del indice de la fila en Cj
                nuevaZj = nuevaZj + nuevamatriz.loc[indicefila] * valorCj                       #Sumar a nuevaZj la multiplicacion de la fila por su valor en Cj
        
        nuevamatriz.loc['Zj'] = nuevaZj                                             #Cambiar el valor de Zj
        nuevamatriz.loc['Cj-Zj'] = nuevamatriz.loc['Cj'] - nuevamatriz.loc['Zj']    #Determinar el valor de Cj - Zj
        nuevamatriz['Sol'].loc['Cj-Zj'] = np.nan                                    #Indeterminar el valor del vector solucion de Cj -Zj
        self.Matrices.append(nuevamatriz)                                           #Agregar la nueva matriz a la lista de matrices

    def printMatrices(self):
        i = 1
        for matriz in self.Matrices:
            print('\n*************\t {} iteracion \t *****************'.format(i))
            print(matriz)
            print('-------------------------------------------------\n')
            i += 1 
    
    def getSolution(self):
        matriz = self.Matrices[-1]
        variables = matriz.columns.values[:-1]
        variablesinRow = matriz.index.values[:-3]
        result = []
        for variable in variables:
            if variable in variablesinRow:
                result.append(matriz['Sol'].loc[variable])
            else:
                result.append(0)
        variables = np.append(variables, ['Zj'])
        result.append(matriz['Sol'].loc['Zj'])
        resultDF = pd.DataFrame([result], index = ['Sol'], columns =variables)
        return resultDF
    def getSolutionJSON(self):
        data = self.getSolution()
        return pd.DataFrame.to_json(data,orient="table")
    
    def getMatrices(self):
        return self.Matrices

    def getMatricesJSON(self):
        auxm=[]
        for m in self.Matrices:
            auxm.append(pd.DataFrame.to_json(m,orient='table'))
        return auxm


if __name__ == "__main__":
    data = {
        'objetivo': 'Maximizar',
        'noVariables': 2,
        'funcionObjetivo': [ 2, 2 ],
        'restricciones': [ [0, 1, 2, 50], [1, 1, 1, 10], [0, 3, 1, 60] ]
    }
    solSimplex = solverSimplex(data)
    solSimplex.solveProblem()
    solSimplex.printMatrices()
    #print(solSimplex.getSolution)