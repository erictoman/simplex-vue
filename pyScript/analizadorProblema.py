#Clase que determina la primera Matriz de coeficientes para el metodo simplex 3
#Realizado por: Luis Fernando Martinez Rodriguez
#Fecha de realizacion: 19-Marzo-2019

import pandas as pd
import numpy as np

class analizadorProblemaSimplex():
    def __init__(self, data):
        self.tipo = data['objetivo'] #Obtener el tipo de objetivo "Maximizar" o "Minimizar"
        self.noVariables = data['noVariables'] #Numero de variables del problema lineal
        self.funcionObjetivo = data['funcionObjetivo'] #Obtener la funcion objetivo
        self.restricciones = self.fixRestricciones(data['restricciones']) #Modificar las restricciones, para aplicar simplex 3
        self.noRestricciones = len( self.restricciones ) #Obtener el numero de restricciones uma vez modificadas las originales
        self.createFirstMatrix() #Crear la primera matriz de iteracion de simplex
    
    def get_First_Matrix(self):
        return self.firstMatrix #Retorna la primera matriz de simplex
    
    def createFirstMatrix(self):
        variables = [] 
        holguras = []
        for i in range(0, self.noVariables): #Numerar el numero de las variables
            variables.append('X{}'.format(i))
        for i in range(0, self.noRestricciones): #Numerar las holguras que se agregaran al problema
            holguras.append('h{}'.format(i))
        self.columns = variables + holguras + ['Sol'] #Definir las columnas como el conjunto de las las variables, holguras y la solucion
        self.rows = holguras.copy() + ['Cj', 'Zj', 'Cj-Zj']

        matrix = self.createMatrix() #Definir los coeficientes de la primera matriz simplex
        self.firstMatrix = pd.DataFrame(matrix, index=self.rows, columns=self.columns)  #Crear un DataFrame con las columnas e indices de la primera matriz simplex
        self.firstMatrix['Sol'].loc['Cj-Zj'] = np.nan                                   #Indeterminar el valor del vector solucion de Cj -Zj

    def fixRestricciones(self, restricciones):
        nuevasRestricciones = []
        for restriccion in restricciones: #Se realiza por cada restriccion existente en el conjunto restricciones
            restriccionesAuxiliares = [] #restricciones auxiliares, por si existe una restricion del tipo X0 + ... + Xn = N
            if(restriccion[0] == 2): #Restriccion del tipo X0 + ... Xn = N, se debe crear nuevas restricciones
                restriccionesAuxiliares.append([0] + restriccion[1:]) #Restriccion del tipo X0 + ... + Xn <= N
                restriccionesAuxiliares.append([1] + restriccion[1:]) #Restriccion del tipo X0 + ... + Xn >= N
            else:
                restriccionesAuxiliares.append(restriccion) #Se agrega la restriccion original
            
            for restriccionAux in restriccionesAuxiliares: #Se realiza por cada restriccion existente en el conjunto restriccionesAuxiliares
                aux = 1 #variable auxiliar que sirve para cambiar el signo de la restriccion
                if(restriccionAux[0] == 1): #Restriccion del tipo X0 + ... + Xn >= N
                    aux = -1 #La varaible auxiliar cambia de signo para crear una restriccion del tipo  - X0 - ... - Xn <= - N
                nparray = np.array(restriccionAux[1:]) * aux #Se crea un numpy array y se multiplica por la variable auxiliar
                nuevasRestricciones.append(nparray) #Se agrega el numpy array a nuevasRestricciones
        return nuevasRestricciones

    def createMatrix(self):
        matrix = [] #Variable que contiene los coeficientes de la primera matriz de simplex
        holguras = [1] + [ 0 for x in range(0, self.noRestricciones - 1) ] #Se crea un nueva lista que contiene los coeficientes de las holguras de la primera restriccion
        for i in range(0, self.noRestricciones): #Se realiza un por el total de restricciones
            #Se concatenan los coeficientes de las restricciones, se rota la lista de las holguras, y se concatena el vector solucion
            matrix.append( np.concatenate( ( self.restricciones[i][:-1], np.roll(holguras, i), [ self.restricciones[i][-1] ] ) , axis=0 ) )
        matrix.append(np.append( self.funcionObjetivo, [0 for i in range (0,self.noRestricciones+1)] )) #Se agrega la funcion objetivo y sus coeficientes en holgura que son 0
        matrix.append(np.array( [ 0 for x in range(0, len( self.columns ) ) ] ) ) #Se agrega el valor de Cj que en la primera matriz de simplex es 0
        matrix.append(matrix[-2] - matrix[-1]) #Se realiza la operacion Cj - Zj
        return matrix
    
    def get_Objetivo(self):
        return self.tipo #Retorna el objetivo del problema


if __name__ == "__main__":
    def main():
        #Se define  un diccionario que contiene las llaves(keys):
        #  tipo             => Determina si el objetivo es "Maximizar" o "Minimizar"
        #  noVariables      => Indica el numero de variables de problema lineal 
        #  funcionObjetivo  => Es una lista que contiene los coeficientes de la funcion objetivo 
        #  restricciones    => Es una lista de listas que contiene el tipo de restriccion, y los coeficientes y vector solucion de la misma
        #                      Primer elemento indica el tipo de restriccion; 0 => menor igual | 1 => mayor igual | 2 => igual
        # data = {
        #     'objetivo': 'Maximizar',
        #     'noVariables': 2,
        #     'funcionObjetivo': [ 1, 1 ],
        #     'restricciones': [ [1, 2, 1, 3], [0, 1, 2, 10] ]
        # }
        data = {
            'objetivo': 'Maximizar',
            'noVariables': 3,
            'funcionObjetivo': [ 3, 2, 4 ],
            'restricciones': [ [1, 3, 4, 0, 10], [0, 6, 0, 5, 30], [2, 3, 4, 4, 15] ]
        }
        aP = analizadorProblemaSimplex(data)
        Matriz = aP.get_First_Matrix()
        print(Matriz)
        print(Matriz[ Matriz['Sol'] != 0])

    main()