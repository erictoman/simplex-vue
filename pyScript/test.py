from solver import solverSimplex as sS
import json
import sys

if __name__ == '__main__':
    data = sys.argv[1]
    data = json.loads(data)
    solSimplex = sS(data)
    solSimplex.solveProblem()
    data = {
        "Indeterminado": solSimplex.Indeterminado,
        "Solucion": solSimplex.getSolutionJSON(),
        "Matrices": solSimplex.getMatricesJSON(),
        "Resuelto": solSimplex.status
    }
    print(json.dumps(data))
